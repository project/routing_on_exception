# Routing On Exception

This is a utility module that adds the ability to specify actions that should
be taken on your route when an HttpException occurs on it. A common example
would be, when an access check fails and an AccessDeniedException is returned.

Obviously you can write your own EventSubscriber to handle your case, but I
think for common/straightforward actions (i.e. redirecting the user on an
access denied response), declaring it alongside the route is clearer.

## Example - CSRF tokens

One example is if you want to add a CSRF Token to your route, but you'd rather
the user was redirected if this validation fails, rather than being left on an
Access Denied page.

Your my_module.routing.yml may look like this:

    my_module.listing_route:
      path: '/my/listing/path'
      defaults:
        _controller: '\Drupal\my_module\Controller\CoolController::listThings'
      methods: [ GET ]

    my_module.action_route:
      path: '/my/listing/path/take_action'
      defaults:
        _controller: '\Drupal\my_module\Controller\CoolController::takeAction'
      methods: [ GET ]
      options:
        on_exception:
         - on:
             - name: '.*AccessDeniedHttpException$'
               message: '.*csrf_token.*'
           then:
             - log: 'Oh noes, this should not happen but it did.'
             - redirect: my_module.listing_route
        requirements:
          _csrf_token: 'TRUE'

Or you could redirect all exceptions regardless of what they are:

    my_module.action_route:
      path: '/my/listing/path/take_action'
      defaults:
        _controller: '\Drupal\my_module\Controller\CoolController::takeAction'
      methods: [ GET ]
      options:
        on_exception:
          - then:
              - redirect: my_module.listing_route
        requirements:
          _csrf_token: 'TRUE'

## Rules
For an on_exception rule to match at least one of its 'on' conditions must
match all of its conditions.

More than one rule can be specified for the same route. The first rule that
matches will have all of its actions executed.

## Todo

If others find this helpful, we should probably refactor this module to use
proper Condition and Action Plugins rather than all the switch statements.
