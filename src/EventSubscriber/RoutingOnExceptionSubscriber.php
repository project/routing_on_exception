<?php

namespace Drupal\routing_on_exception\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Url;
use Drupal\routing_on_exception\RoutingOnExceptionManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirects users when access is denied due to CSRF Token validation failure.
 */
class RoutingOnExceptionSubscriber implements EventSubscriberInterface {
  /**
   * Route exception redirect manager.
   *
   * @var \Drupal\routing_on_exception\RoutingOnExceptionManagerInterface
   */
  private $routeExceptionRedirectsManager;

  /**
   * Logger channel interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Constructs a RoutingOnExceptionSubscriber object.
   *
   * @param \Drupal\routing_on_exception\RoutingOnExceptionManagerInterface $routeExceptionRedirectsManager
   *   The routing exception redirects manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel interface.
   */
  public function __construct(
    RoutingOnExceptionManagerInterface $routeExceptionRedirectsManager,
    LoggerChannelInterface $logger
  ) {
    $this->routeExceptionRedirectsManager = $routeExceptionRedirectsManager;
    $this->logger = $logger;
  }

  /**
   * Validates the 'on_exception' option for routes.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onRouterAlterValidateOnExceptionOption(RouteBuildEvent $event) {
    $routes = $event->getRouteCollection();
    $this->routeExceptionRedirectsManager->validateOnExceptionsOption($routes);
  }

  /**
   * Applies routing on exception actions.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The exception event.
   */
  public function onExceptionApplyRoutingOnExceptionActions(ExceptionEvent $event) {
    $rule_actions = $this->routeExceptionRedirectsManager->getRouteExceptionActions(
      RouteMatch::createFromRequest($event->getRequest()),
      $event->getThrowable()
    );
    foreach ($rule_actions as $actions) {
      foreach ($actions as $action => $value) {
        switch ($action) {
          case 'log':
            $this->logger->error($value);
            break;

          case 'redirect':
            $event->setResponse(new RedirectResponse(Url::fromRoute($value)->toString()));
            break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Use a lower priority than
    // \Drupal\Core\EventSubscriber\ExceptionLoggingSubscriber, so Drupal still
    // logs access denied errors.
    $events[KernelEvents::EXCEPTION][] = [
      'onExceptionApplyRoutingOnExceptionActions',
      40,
    ];
    $events[RoutingEvents::ALTER][] = [
      'onRouterAlterValidateOnExceptionOption',
      0,
    ];
    return $events;
  }

}
