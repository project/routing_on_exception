<?php

namespace Drupal\routing_on_exception;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Manages routing on exception behavior.
 *
 * @todo Make this use plugins for different tests / actions.
 */
class RoutingOnExceptionManager implements RoutingOnExceptionManagerInterface {

  /**
   * Validates the 'on_exception' option for routes.
   *
   * @param \Symfony\Component\Routing\RouteCollection $routes
   *   The collection of routes to validate.
   *
   * @throws \Drupal\routing_on_exception\RoutingOnExceptionDefinitionException
   *   Thrown when the route definition is invalid.
   */
  public function validateOnExceptionsOption(RouteCollection $routes) {
    foreach ($routes->all() as $route_name => $route) {
      if (!$on_exception_rules = $route->getOption('on_exception')) {
        continue;
      }
      $yaml_path = "$route_name.options.on_exception";
      if (!static::isNonEmptyNumericArray($on_exception_rules)) {
        throw new RoutingOnExceptionDefinitionException(strtr(
          'Invalid route definition: If it exists, :yaml_path must be a non-empty numeric array, :type given.',
          [':yaml_path' => $yaml_path, ':type' => gettype($on_exception_rules)]
        ));
      }
      foreach ($on_exception_rules as $key => $on_exception_rule) {
        $this->validateOnExceptionRule($on_exception_rule, $yaml_path . '[' . $key . ']', $routes);
      }
    }
  }

  /**
   * Validates an individual 'on_exception' rule.
   *
   * @param array $on_exception_rule
   *   The 'on_exception' rule to validate.
   * @param string $yaml_path
   *   The YAML path to the rule.
   * @param \Symfony\Component\Routing\RouteCollection $all_routes
   *   The collection of all routes.
   *
   * @throws \Drupal\routing_on_exception\RoutingOnExceptionDefinitionException
   *   Thrown when the rule is invalid.
   */
  private function validateOnExceptionRule(array $on_exception_rule, string $yaml_path, RouteCollection $all_routes) {
    if (!static::isNonEmptyAssociativeArray($on_exception_rule)) {
      throw new RoutingOnExceptionDefinitionException(strtr(
        'Invalid route definition: :yaml_path must be a non-empty associative array, :type given.',
        [':yaml_path' => $yaml_path, ':type' => gettype($on_exception_rule)]
      ));
    }
    $this->validateOn($on_exception_rule, $yaml_path);
    $this->validateThen($on_exception_rule, $yaml_path, $all_routes);
  }

  /**
   * Validates the 'on' condition of an 'on_exception' rule.
   *
   * @param array $on_exception_rule
   *   The 'on_exception' rule to validate.
   * @param string $yaml_path
   *   The YAML path to the rule.
   *
   * @throws \Drupal\routing_on_exception\RoutingOnExceptionDefinitionException
   *   Thrown when the 'on' condition is invalid.
   */
  private function validateOn(array $on_exception_rule, string $yaml_path) {
    if (!array_key_exists('on', $on_exception_rule)) {
      return;
    }
    if (!static::isNonEmptyNumericArray($on_exception_rule['on'])) {
      throw new RoutingOnExceptionDefinitionException(strtr(
        'Invalid route definition: :yaml_path.on must be a non-empty numeric array, :type given.',
        [
          ':yaml_path' => $yaml_path,
          ':type' => gettype($on_exception_rule['on']),
        ]
      ));
    }
    foreach ($on_exception_rule['on'] as $key => $on) {
      if (!static::isNonEmptyAssociativeArray($on)) {
        throw new RoutingOnExceptionDefinitionException(strtr(
          'Invalid route definition: :yaml_path.on[:key] must be a non-empty associative array, :type given.',
          [':yaml_path' => $yaml_path, ':key' => $key, ':type' => gettype($on)]
        ));
      }
      $valid_keys = ['name', 'message'];
      $invalid_keys = array_diff(array_keys($on), $valid_keys);
      if (count($invalid_keys) > 0) {
        throw new RoutingOnExceptionDefinitionException(strtr(
          'Invalid route definition: :yaml_path.on[:key] must contain only the keys :valid_keys, but it seems to contain :invalid_keys.',
          [
            ':yaml_path' => $yaml_path,
            ':key' => $key,
            ':valid_keys' => '"' . implode('","', $valid_keys) . '"',
            ':invalid_keys' => '"' . implode('","', $invalid_keys) . '"',
          ]
        ));
      }
      foreach ($on as $condition => $value) {
        if (!static::isValidRegularExpression($value)) {
          throw new RoutingOnExceptionDefinitionException(strtr(
            'Invalid route definition: :yaml_path.on[:key].:condition must be a valid regular expression, ":value" given.',
            [
              ':yaml_path' => $yaml_path,
              ':key' => $key,
              ':condition' => $condition,
              ':value' => $value,
            ]
          ));
        }
      }
    }
  }

  /**
   * Validates the 'then' action of an 'on_exception' rule.
   *
   * @param array $on_exception_rule
   *   The 'on_exception' rule to validate.
   * @param string $yaml_path
   *   The YAML path to the rule.
   * @param \Symfony\Component\Routing\RouteCollection $all_routes
   *   The collection of all routes.
   *
   * @throws \Drupal\routing_on_exception\RoutingOnExceptionDefinitionException
   *   Thrown when the 'then' action is invalid.
   */
  private function validateThen(array $on_exception_rule, string $yaml_path, RouteCollection $all_routes) {
    if (!array_key_exists('then', $on_exception_rule)) {
      throw new RoutingOnExceptionDefinitionException(strtr(
        'Invalid route definition: :yaml_path must contain the key "then".',
        [':yaml_path' => $yaml_path]
      ));
    }
    if (!static::isNonEmptyNumericArray($on_exception_rule['then'])) {
      throw new RoutingOnExceptionDefinitionException(strtr(
        'Invalid route definition: :yaml_path.then must be a non-empty numeric array, :type given.',
        [
          ':yaml_path' => $yaml_path,
          ':type' => gettype($on_exception_rule['then']),
        ]
      ));
    }
    foreach ($on_exception_rule['then'] as $key => $then) {
      if (!static::isNonEmptyAssociativeArray($then)) {
        throw new RoutingOnExceptionDefinitionException(strtr(
          'Invalid route definition: :yaml_path.then[:key] must be a non-empty associative array, :type given.',
          [
            ':yaml_path' => $yaml_path,
            ':key' => $key,
            ':type' => gettype($then),
          ]
        ));
      }
      $valid_keys = ['log', 'redirect'];
      $invalid_keys = array_diff(array_keys($then), $valid_keys);
      if (count($invalid_keys) > 0) {
        throw new RoutingOnExceptionDefinitionException(strtr(
          'Invalid route definition: :yaml_path.then[:key] must contain only the keys :valid_keys, but it seems to contain :invalid_keys.',
          [
            ':yaml_path' => $yaml_path,
            ':key' => $key,
            ':valid_keys' => '"' . implode('","', $valid_keys) . '"',
            ':invalid_keys' => '"' . implode('","', $invalid_keys) . '"',
          ]
        ));
      }
      foreach ($then as $action => $value) {
        switch ($action) {
          case 'log':
            if (!is_string($value)) {
              throw new RoutingOnExceptionDefinitionException(strtr(
                'Invalid route definition: :yaml_path.on[:key].:action must be string, :type given.',
                [
                  ':yaml_path' => $yaml_path,
                  ':key' => $key,
                  ':action' => $action,
                  ':type' => gettype($value),
                ]
              ));
            }
            break;

          case 'redirect':
            if (!is_string($value)) {
              throw new RoutingOnExceptionDefinitionException(strtr(
                'Invalid route definition: :yaml_path.on[:key].:action must be a string, :type given.',
                [
                  ':yaml_path' => $yaml_path,
                  ':key' => $key,
                  ':action' => $action,
                  ':type' => gettype($value),
                ]
              ));
            }
            if ($all_routes->get($value) === NULL) {
              throw new RoutingOnExceptionDefinitionException(strtr(
                'Invalid route definition: :yaml_path.on[:key].:action must be the name of a valid route, :value given.',
                [
                  ':yaml_path' => $yaml_path,
                  ':key' => $key,
                  ':action' => $action,
                  ':value' => $value,
                ]
              ));
            }
            break;
        }
      }
    }
  }

  /**
   * Checks if a value is a non-empty associative array.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   TRUE if the value is a non-empty associative array, FALSE otherwise.
   */
  private static function isNonEmptyAssociativeArray($value): bool {
    return static::isAssociativeArray($value) && count($value) > 0;
  }

  /**
   * Checks if a value is a non-empty numeric array.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   TRUE if the value is a non-empty numeric array, FALSE otherwise.
   */
  private static function isNonEmptyNumericArray($value): bool {
    return static::isNumericArray($value) && count($value) > 0;
  }

  /**
   * Checks if a value is an associative array.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   TRUE if the value is an associative array, FALSE otherwise.
   */
  private static function isAssociativeArray($value): bool {
    return is_array($value) && count(array_filter(array_keys($value), 'is_numeric')) === 0;
  }

  /**
   * Checks if a value is a numeric array.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   TRUE if the value is a numeric array, FALSE otherwise.
   */
  private static function isNumericArray($value): bool {
    return is_array($value) && count(array_filter(array_keys($value), 'is_numeric')) === count($value);
  }

  /**
   * Checks if a value is a valid regular expression.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   TRUE if the value is a valid regular expression, FALSE otherwise.
   */
  private static function isValidRegularExpression($value): bool {
    return is_string($value) && @preg_match('~' . strtr($value, ['~' => '\~']) . '~', '') !== FALSE;
  }

  /**
   * Get the exception actions for a route's 'on_exception' option.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   * @param \Throwable $exception
   *   The exception object.
   *
   * @return array
   *   An array of exception actions.
   */
  public function getRouteExceptionActions(RouteMatchInterface $route_match, \Throwable $exception): array {
    $route = $route_match->getRouteObject();
    if ($route === NULL) {
      return [];
    }
    if (!$on_exception_rules = $route->getOption('on_exception')) {
      return [];
    }

    foreach ($on_exception_rules as $on_exception_rule) {
      if ($this->ruleMatches($on_exception_rule, $exception)) {
        return $on_exception_rule['then'];
      }
    }

    return [];
  }

  /**
   * Checks if a given exception matches the specified rule.
   *
   * @param mixed $on_exception_rule
   *   The rule to check.
   * @param \Throwable $exception
   *   The exception object to match against the rule.
   *
   * @return bool
   *   TRUE if the rule matches the exception, FALSE otherwise.
   */
  private function ruleMatches($on_exception_rule, \Throwable $exception): bool {
    // Allow the omission of 'on' for universal exception matching.
    if (!isset($on_exception_rule['on'])) {
      return TRUE;
    }
    // At least one 'on' condition must match for rule matching (OR operation).
    foreach ($on_exception_rule['on'] as $on) {
      // All 'on' conditions must match (AND operation) for a match to occur.
      foreach ($on as $on_condition => $value) {
        $value_regex = '~' . strtr($value, ['~' => '\~']) . '~';
        switch ($on_condition) {
          case 'name':
            if (!preg_match($value_regex, get_class($exception))) {
              // We already know this 'on' has failed since one of its
              // conditions failed--move to the next.
              continue 3;
            }
            break;

          case 'message':
            if (!preg_match($value_regex, $exception->getMessage())) {
              // We already know this 'on' has failed since one of its
              // conditions failed--move to the next.
              continue 3;
            }
            break;
        }
      }
      // This 'on' has passed all its conditions. That is enough for the whole
      // rule to match.
      return TRUE;
    }
    return FALSE;
  }

}
