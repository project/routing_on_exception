<?php

namespace Drupal\routing_on_exception;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Interface for managing routing on exception functionality.
 */
interface RoutingOnExceptionManagerInterface {

  /**
   * Validates each routes' on_exception option.
   */
  public function validateOnExceptionsOption(RouteCollection $routes);

  /**
   * Get route exception actions for the current route and thrown exception.
   */
  public function getRouteExceptionActions(RouteMatchInterface $route_match, \Throwable $exception): array;

}
