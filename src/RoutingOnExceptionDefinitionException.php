<?php

namespace Drupal\routing_on_exception;

/**
 * Exception thrown for routing on exception definition errors.
 */
class RoutingOnExceptionDefinitionException extends \Exception {
}
